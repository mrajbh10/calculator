import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;    //for event listening for the button press
import java.io.IOException; //for checked exception handelling
import java.util.Scanner;   //for scanning input form the user 
import java.util.Arrays;    //for arrays 
import java.util.Stack;     //for stack
import java.util.ArrayList; //for arraylist

/**
 * Calculator program for the Assignment 2 - 
 * Prog5001-2021-3 Fundamentals of programming.
 *
 * @author Manik Rajbhandari
 * @version V.1
 */
public class MyCalc extends JFrame implements ActionListener {
    // instance variables - for the calculation purpose
    
    //instance local variables  of class MyCalc- for the GUI
    JTextField display; //calculator's display
    JButton[] buttonsNo;
    JButton[] buttonsOperand;
    JButton[] buttonsUtility;
    JButton backSpace;
    JButton equalsTo;
    //for no.s in left panel
    String[] buttonNamesNo = {"1","2","3","4","5","6","7","8","9","+/-","0","."};
    String[] buttonNamesOperand = {"+", "-", "*", "/", "!"};
    String[] buttonNamesUtility = {"<<","C","(",")","OFF"};
    String[] buttonCommandsNo = {"CMD_1","CMD_2","CMD_3","CMD_4","CMD_5",
        "CMD_6","CMD_7","CMD_8","CMD_9","CMD_INV","CMD_0","CMD_DOT"};
    String[] buttonCommandsOperand = {"CMD_ADD","CMD_SUB","CMD_MUL","CMD_DIV","CMD_FAC"};
    String[] buttonCommandsUtility = {"CMD_BSP","CMD_CE","CMD_LBR","CMD_RBR","CMD_OFF"};
        
        
    /**
     * Constructor for objects of class SimpleCalc
     */
    public MyCalc(){
        //for initializing/generating gui first
        super("My PROG5001 - Calculator(V 1.0)");
        setSize(400, 500);
        createCalcGUI();
        
    }

    /**
     * A method to set up the GUI
     * All the layout initialization and configuration is done here
     * All the Buttons on the layout is created here using the global variables for 
     * each panels respectively.
    */
    private void createCalcGUI() {
        //top panel initialize
        JPanel panelTop = new JPanel();
        panelTop.setBackground(Color.gray);
        GridLayout panelTopLayout = new GridLayout(0,1);
        panelTop.setLayout(panelTopLayout);
        //left panel initialize
        JPanel panelLeft = new JPanel();
        panelLeft.setBackground(Color.gray);        
        GridLayout panelLeftLayout = new GridLayout(5,3);
        panelLeftLayout.setHgap(4);
        panelLeftLayout.setVgap(4);
        panelLeft.setLayout(panelLeftLayout);        
        //
        //mid panel initialize 
        JPanel panelMid = new JPanel();
        panelMid.setBackground(Color.gray);  
        GridLayout panelMidLayout = new GridLayout(5,1);
        panelMidLayout.setHgap(4);
        panelMidLayout.setVgap(4);
        panelMid.setLayout(panelMidLayout);        
        //
        //right panel initialize 
        JPanel panelRight = new JPanel();
        panelRight.setBackground(Color.gray);        
        GridLayout panelRightLayout = new GridLayout(5,1);
        panelRightLayout.setHgap(4);
        panelRightLayout.setVgap(4);
        panelRight.setLayout(panelRightLayout);        
        //
        BorderLayout mainLayout = new BorderLayout();
        setLayout(mainLayout);
        add(panelTop, BorderLayout.PAGE_START);
        add(panelLeft, BorderLayout.WEST);
        add(panelMid, BorderLayout.CENTER);
        add(panelRight, BorderLayout.EAST);
        //create display
        display = new JTextField("0");        
        Font displayFont = new Font("SansSerif", Font.BOLD, 24);
        display.setFont(displayFont);
        display.setHorizontalAlignment(JTextField.RIGHT);        
        display.setPreferredSize(new Dimension(100,90));
        panelTop.add(display);
        //create no buttons
        buttonsNo = new JButton[12];
        for (int i = 0; i < buttonsNo.length; i++) {
            buttonsNo[i] = new JButton();
            buttonsNo[i].setText(buttonNamesNo[i]);
            buttonsNo[i].setActionCommand(buttonCommandsNo[i]);
            buttonsNo[i].addActionListener(this);
            panelLeft.add(buttonsNo[i]);
        }
        //create operand buttons
        buttonsOperand = new JButton[5];
        for (int i = 0; i < buttonsOperand.length; i++) {
            buttonsOperand[i] = new JButton();
            buttonsOperand[i].setText(buttonNamesOperand[i]);
            buttonsOperand[i].setActionCommand(buttonCommandsOperand[i]);
            buttonsOperand[i].addActionListener(this);
            panelMid.add(buttonsOperand[i]);
        }
        //create utility buttons
        buttonsUtility = new JButton[5];
        for (int i = 0; i < buttonsUtility.length; i++) {
            buttonsUtility[i] = new JButton();
            buttonsUtility[i].setText(buttonNamesUtility[i]);
            buttonsUtility[i].setActionCommand(buttonCommandsUtility[i]);
            buttonsUtility[i].addActionListener(this);
            if(i == 4){
                JButton offButton = buttonsUtility[i];
                offButton.setForeground(Color.RED);
            }
            panelRight.add(buttonsUtility[i]);
        }
        
        //for Equal Button
        equalsTo = new JButton("=");
        equalsTo.setPreferredSize(new Dimension(40, 100));
        equalsTo.addActionListener(this);
        equalsTo.setActionCommand("CMD_EQL");
        panelLeft.add(equalsTo);
    }
    
    /**
     * A method to perform event trigger operation of different buttons
     *
     * @param  ActionEvent e, and object for event listening.
     */
    public void actionPerformed(ActionEvent e) {
        String displayText;
        String postfix;
        displayText = display.getText();
        String command = e.getActionCommand();
        
        if (command.equals("CMD_1")) {
            displayText = displayText + "1";
        } else
        if (command.equals("CMD_2")) {
            displayText = displayText + "2";
        } else
        if (command.equals("CMD_3")) {
            displayText = displayText + "3";
        } else
        if (command.equals("CMD_4")) {
            displayText = displayText + "4";
        } else
        if (command.equals("CMD_5")) {
            displayText = displayText + "5";
        } else
        if (command.equals("CMD_6")) {
            displayText = displayText + "6";
        } else
        if (command.equals("CMD_7")) {
            displayText = displayText + "7";
        } else
        if (command.equals("CMD_8")) {
            displayText = displayText + "8";
        } else
        if (command.equals("CMD_9")) {
            displayText = displayText + "9";
        } else
        if (command.equals("CMD_INV")) {
            displayText = "-"+ displayText;
        } else
        if (command.equals("CMD_0")) {
            displayText = displayText + "0";
        } else
        if (command.equals("CMD_DOT")) {
            displayText = displayText + ".";
        } else
        if (command.equals("CMD_ADD")) {
            displayText = displayText + "+";
        } else
        if (command.equals("CMD_SUB")) {
            displayText = displayText + "-";
        }  else
        if (command.equals("CMD_MUL")) {
            displayText = displayText + "*";
        }  else
        if (command.equals("CMD_DIV")) {
            displayText = displayText + "/";
        } else
        if (command.equals("CMD_FAC")) {
            displayText = displayText + "!";
        } else
        if (command.equals("CMD_LBR")) {
            displayText = displayText + "(";
        }  else
        if (command.equals("CMD_RBR")) {
            displayText = displayText + ")";
        } else
        if (command.equals("CMD_OFF")) {
            //displayText = displayText + ".";
            System.exit(0);
        } else
        if (command.equals("CMD_CE")) {
            displayText = "";
        } else
        if (command.equals("CMD_BSP")) {
            int len = displayText.length();
            if ( len > 0 ){
                displayText = displayText.substring(0, len-1);
            }
        } else
        if (command.equals("CMD_EQL")){
            displayText = displayText + " = "+ showResult(displayText);
        }
        display.setText(displayText);
    }

        
    /**
     * A method to perform addition operation of two no.s
     *
     * @param  first and second two double parameter for a method
     * @return the sum of first and second
     */
    public double addition(double first, double second)
    {
        //perform the addition
        double temp = 0.0;
        temp = first + second;
        return temp;
    }
    
    /**
     * A method to perform subtraction operation of two no.s
     *
     * @param  first and second two double parameter for a method
     * @return the difference of first and second
     */
    public double subtraction(double first, double second)
    {
        //perform the subtraction
        double temp = 0.0 ;
        temp = first - second;
        return temp;
    }
    
    /**
     * A method to perform multiplication operation of two no.s
     *
     * @param  first and second two double parameter for a method
     * @return the multiplication of first with second
    */
    public double multiplication(double first, double second)
    {
        //perform the subtraction
        double temp = 0.0;
        temp = first * second;
        return temp ;
    }
    
    /**
     * A method to perform division operation of two no.s
     *
     * @param  first and second parameter for a method
     * @return    the division of first by second
     */
    public double division(double first, double second)
    {
        double divResult = 0.0; ;
        if (first != 0 && second == 0){
            System.out.println("!!Second Number can't be 0, try again.!!");
        }
        else if (first == 0 && second == 0){
            System.out.println("!!Both Number can't be 0, try again.!!");
        }
        else {
            //perform the division
            divResult = first / second;
        }   
        
        return divResult ;
    }
    
    
    
    /**
     * A method to perform factorial operation of a number < 10
     *
     * @param  double inpur as x
     * @return    the factorial of n is: n!
     */
    public double factorial(double x){
        //perform the factorial
        //variables for factorial
        double number = x;
        double temp = 1; //temp fact result set 1 for handeling 0! == 1

        for (double i=number; i>=1; i--){
            temp = temp * i ;
        }
        
        return temp ;        
    }

    /**
     * A method to perform operator hierarchy/preceddence of operator 
     *
     * @param  String C, input
     * @return  integer  no. like 1, 2, 3 and a negative -1 for individual cases.
     */
      static int precedence(String c){
        switch (c){
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            case "!":
                return 3;
        }
        return -1;
    }
    
    
    /**
     * A method to perform conversion of the user input/display expression infix to postfix
     *
     * @param  String expression,passed from the showResult method
     * @return  the postfix expression as an arraylist
     */
     static ArrayList<String> infixToPostFix(String expression){

        ArrayList<String> result = new ArrayList<String>();
        
        String regex = "(?<=op)|(?=op)".replace("op", "[-+*/()!]");

        //For the future reference
        // actual regex becomes (?<=[-+*/()])|(?=[-+*/()])
        //[…] is a character class definition
        //(?<=…) is a lookbehind; it asserts that we can match … to the left
        //(?=…) is a lookahead; it asserts that we can match … to the right
        //this|that is alternation
        //Thus, (?<=op)|(?=op) matches everywhere after or before op
        //... where op is replaced by [-+*/()], i.e. a character class that matches operators
        //Note that - is first here so that it doesn't become a range definition meta character
        
        String[] outs = expression.split(regex); //array to store no. and operands 
        System.out.println(Arrays.toString(outs));
        
        Stack<String> stack = new Stack();
        
        for (int i = 0; i < outs.length ; i++) {
            String c = outs[i];
            System.out.println(c);

            //check if string c is operator
            if(precedence(c)>0){
                //operator
                while (!stack.isEmpty() && precedence(c) <= precedence(stack.peek())) {
                    result.add(stack.pop());
                }
                stack.push(c);
            }else if(c.equals(")")){
                 while (!stack.isEmpty() && !stack.peek().equals("(")) {
                    result.add(stack.pop());
                }
                stack.pop(); //take out the left parenthesis from the stack
            }else if(c.equals("(")){
                stack.push(c);
            }else{
                //character is neither operator nor ( 
                result.add(c);
            }
        }
        
        while (!stack.isEmpty()) {
            result.add(stack.pop());
        }
        
        return result;
       
    }
    
    /**
     * A method to perform evaluation of the postfix arraylist
     *
     * @param  arraylist postfix,
     * @return double result; the final result of the user input expression
     */
      public double evaluate(ArrayList<String> postfix) { 
        Stack<Double> stack = new Stack();

        double operand2 = 0.0;
        double operand1 = 0.0;
        double result = 0;
        
        
        for (int i = 0; i < postfix.size(); i++) {
            String c = postfix.get(i);
    
            if (precedence(c) > 0) {
                if( precedence(c) != 3){
                    operand2 = Double.parseDouble("" + stack.pop());
                    operand1 = Double.parseDouble("" + stack.pop()); 
                }else {
                    operand1 = Double.parseDouble("" + stack.pop()); 
                }
                
                if (c.equals("+")) {
                    result = addition(operand1,operand2);
                } else
                if (c.equals("-")) {
                    result = subtraction(operand1,operand2);
                } else
                if (c.equals("*")) {
                    result = multiplication(operand1,operand2);
                } else
                if (c.equals("/")) {
                    result = division(operand1,operand2);
                } else
                if (c.equals("!")){
                    result = factorial(operand1);
                }
                stack.push(result);
            } else {
                //c is an operand
                stack.push(Double.parseDouble(c));                
            }  
        }
        
        result = stack.pop();
        return result;
    }
    
    /**
     * A method to show the result which is called after pressing equalsto sign of the GUI
     * it calls the infixToPostfix method for the conversion and again 
     * stores the result by calling the evaluate method
     *
     * @param  String text; an expression form the display
     * @return double result; the final result of 
     * the user input expression got form the evaluate method
     */
    public String showResult (String text){
        String result = "";
        String displayText = text ;
        ArrayList<String> postfix; 
        
        clearScreen();
        
        postfix = infixToPostFix(displayText);
        result = Double.toString(evaluate(postfix));
        
        return result ;
    }
    
    /**
     * A method to perform clearing of the terminal for debugging purpose
     *
     * @param  void
     * @return void
     */
    public static void clearScreen() {  
        System.out.print('\u000C');
        System.out.flush();  
    }  

    /**
     * The main method to initialize the class object 
     *
     * @param  String[] args for the static seperate call 
     * @return void
     */
    public static void main (String[] args) {
        MyCalc calc = new MyCalc();
        calc.setVisible(true);
    }
}